
// Using this approach until PHP solution is implemented
var TIAA_ud = TIAA_ud || {};

TIAA_ud = {
	udThemeUrl: '/themes/ud_atom/release_2013-08/',
	globalAssetUrl: '/assets/',
	globalThemeUrl: '/assets/release_2013-08/',
	localUrl: 'assets/',
	oneColWidth: 1200,
	twoColWidth: 940

};

$(document).ready(function(){
	$("#subadmin").on('click',function(){
		$('#adminDetails').val('Serge Kavege - Mgr, Investment Accounting');
	});
	
	$("#advSearchOpt").on("click",function(){
		$("#advdSearch").addClass('visible').removeClass('closed');
		$("#basicSearch").addClass('closed').removeClass('visible');
	});
	$("#regSearchOpt").on("click",function(){
		$("#basicSearch").addClass('visible').removeClass('closed');
		$("#advdSearch").addClass('closed').removeClass('visible');
	});
	
	$(".addRegRow").live("click",function(){
		var currentrow = $('#addRegularRow').find('.clearfix');
		currentrow.find('.addrow').remove();
		var totrows = $('#addRegularRow').find('.clearfix').length;
		var $addrow = $("#srcflds1").html();
		$("#srcflds").find('.clearfix').eq(totrows-1).after($addrow);		
	});
	
	$(".deleteRegularRow").live("click",function(){
		$(this).parents('.clearfix').remove();
		var totrows = $('#addRegularRow').find('.clearfix').length;	
		var lastrow = $("#srcflds").find('.clearfix').eq(totrows-1);
		var $addrowicon = $("#addfldsicon").html();
		var chkAdd = lastrow.find('.five_sixth .lblFieldPairGroup .addrow').length;
		if(chkAdd == 0){
			var lblfld = lastrow.find('.five_sixth .lblFieldPairGroup .delrow').after($addrowicon);
		}
		if(totrows == 1){
			$("#srcflds").find('.five_sixth .lblFieldPairGroup .lblFieldPair').eq(2).after($addrowicon);
		}
	});
});